# Présentation des services K8s



------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

Nous allons discuter des services Kubernetes. Dans cette leçon, nous allons simplement donner un aperçu de ce que sont les services Kubernetes et de ce qu'ils font. Voici ce dont nous allons parler dans cette leçon. Nous allons d'abord répondre à la question : qu'est-ce qu'un service ? Ensuite, nous nous concentrerons sur le concept de routage des services et nous parlerons brièvement des points de terminaison.

# Qu'est-ce qu'un service ?

Un service Kubernetes fournit un moyen d'exposer une application exécutée sous forme d'un ensemble de pods. Cela signifie que nous fournissons essentiellement un moyen abstrait pour les clients d'accéder à nos applications sans avoir besoin de connaître les pods de l'application. Nous pourrions avoir une application composée de plusieurs pods répliqués différents, et nous ne voulons pas nécessairement que nos clients se préoccupent du nombre de pods ou du pod avec lequel ils communiquent. Un service est donc une couche d'abstraction qui permet à nos clients de communiquer avec le service plutôt qu'avec ces pods individuels.

# Routage des services

Plongeons un peu plus profondément pour voir à quoi cela ressemble dans notre cluster Kubernetes. Chaque fois qu'un client fait une demande à un service, le service dirige le trafic vers les pods sous-jacents de manière équilibrée. Nous avons donc plusieurs pods et un seul service qui dirige le trafic vers n'importe lequel de ces pods et équilibre la charge entre eux. Notre client fait une demande à un service, qui est ensuite dirigée vers un pod, et les demandes suivantes sont équilibrées entre les différents pods. Ainsi, le client n'a pas à se soucier de savoir avec quel pod il communique. Il n'a même pas besoin d'être conscient du fait qu'il y a plusieurs pods. Il communique simplement avec ce service, et le service gère tout le routage et l'équilibrage de charge.

# Points de terminaison

Je veux aussi parler du concept de points de terminaison (endpoints). Les points de terminaison sont les entités backend vers lesquelles les services dirigent le trafic. Ainsi, lorsque nous avons les trois pods dont nous parlions plus tôt, chacun de ces trois pods représente un point de terminaison pour le service. Les points de terminaison sont simplement quelque chose dont vous devez être conscient, et l'une des façons dont les points de terminaison peuvent être utiles est qu'ils permettent de déterminer facilement vers quels pods un service dirige le trafic. Si vous avez besoin de savoir combien de pods un service dirige le trafic ou vers quels pods un service dirige le trafic, vous pouvez simplement regarder les points de terminaison de ce service.

# Conclusion

Ceci a été un aperçu rapide des services Kubernetes. Nous avons parlé de ce que sont les services. Nous avons discuté du concept de routage des services et de la manière dont les clients communiquent avec plusieurs pods via un service, et nous avons parlé du concept de points de terminaison. Dans la prochaine vidéo, nous commencerons à examiner ce à quoi ressemble réellement la création d'un service dans notre cluster. C'est tout pour cette leçon. À la prochaine !


# Reference

https://kubernetes.io/docs/concepts/services-networking/service/